import { stripGeneratedFileSuffix } from '@angular/compiler/src/aot/util';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  started = false;
  title = 'STOPWATCH';
  seconds = 0;
  milliseconds = 0;
  intervalId = 0;

  toggle(): void {
    this.started = !this.started;
    if (!this.started) {
      window.clearInterval(this.intervalId);
    } else {
      var self = this;
      this.intervalId = window.setInterval(function() {
        self.milliseconds++;
        if (self.milliseconds % 100 === 0) {
          self.seconds++;
          self.milliseconds = 0;
        }
      }, 10);
    }
  }

  reset(): void {
    window.clearInterval(this.intervalId);
    this.seconds = 0;
    this.milliseconds = 0;
    this.started = false;
  }
}
